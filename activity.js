/*
a. What directive is used by Node.js in loading the modules it needs?
Answer: http
b. What Node.js module contains a method for server creation?
Answer: .createServer
c. What is the method of the http object responsible for creating a server using Node.js?
Answer: require
d. What method of the response object allows us to set status codes and consent type?
Answer: .write
e. Where will console.log() output its content when run in Node.js?
Answer: to the console
*/