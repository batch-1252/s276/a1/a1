


let http = require("http");

let PORT = 3000;

http.createServer((request, response)=>{

	if(request.url == "/homePage"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.write("You are in the Home Page");
		response.end();

	} else if (request.url == "/loginPage"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.write("This is the login Page");
		response.end();

	} else {
		response.writeHead(404, {"Content-Type":"text/plain"});
		response.end("I'm sorry Page Could not be display");
	}

}).listen(PORT);

console.log("Server is Successfully Running")